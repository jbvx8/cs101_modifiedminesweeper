##    CS101
##    Program 8 Algorithm
##    Jackie Batson
##    jbvx8@umkc.edu
##    Created: 5/10/15
##    Due: 5/10/15
##
##    Problem: Create a modified Mine Sweeper game, where the user inputs the number of rows,
##    columns, mines, and show mines.  When a show mine tile is clicked, surrounding mines are
##    shown. The game ends when the user uncovers a mine tile.
##
##    Algorithm:
##    1. Prompt the user to enter the number of rows and columns for the board, separated by a
##       comma. Continue prompting until the correct format is entered. If less than three rows
##       or columns are entered, default to 3.
##    2. Split the entry on the comma and store the values as variables for rows and columns
##    3. Prompt the user to enter the number of mine and show mine tiles.  Continue prompting
##       until valid entries are made, and store the values as variables for mine and show
##       mine tiles.
##    4. Build a board using the SuperSweeperBoard class, passing in the rows, columns, mine
##       tiles, and show mine tiles:
##           a) Place the mines and show mines randomly on the board.
##               b) For remaining tiles, determine the number of mines surrounding each tiles and
##                  place the number on the board
##               c) Set all tiles to hidden and display each as "H"
##    5. Display the board to the user, and prompt the user for a tile to uncover in y,x format.
##       Continue prompting until a valid tile is entered.
##    6. Uncover the chosen tile.
##           a)if it is a mine tile, display an *, uncover all tiles (hidden = False), 
##                 and end the game.
##               b)if it is a show mine tile, show the number of surrounding mines and display  
##                 the surrounding mines as *.
##               c)otherwise, display the number of surrounding mines (hidden = False)
##    7. Repeat steps 5-6 until all numbered tiles are displayed or until a mine is displayed.
##       Show the full uncovered board at the end of the game.
##    8. Ask the user if they want to play again. Continue prompting until a valid entry is
##       made. Repeat steps 1-7 until the user quits.
##
##    Error Handling: set default values for rows, columns, mines, and show mines if user entries
##                    are out of range. Check that user tiles to uncover are less than or equal to
##                    number of rows/columns and greater than 0. Check for valid input on play again
##                    prompt and reprompt for valid input.

import random

class Tile(object):
    """ Represents a single Tile on the board.
            The Tile contains the following information
                board : SuperSweeperBoard.  This is the board th tile is on.
                hidden :  boolean.  True if the tile value is hidden, False if it is showing.
                __value : String. The value to show for the tile as string output ( assuming it's not hidden )
                row : int.  The row this tile is on the board.
                col : int.  The column this tile belongs to on the board.
                __mine_count : int.  The number of mines surrounding this tile. """

    
    def __init__(self, mine_board, row, col):
        """ Initializes the Tile object.
        :param mine_board: The board this tile is on.
        :param row: The row this tile occupies
        :param col: The col this tile occupies
        :return: Nothing

        Modifications : Sets the instance to the proper state on starting.

        Errors Handled : None
        """
        
        self.mine_board = mine_board
        self.row = row
        self.col = col
        self.hidden = True
        self.__value = "0"
        self.__mine_count = 0
        

    def __str__(self):
        """  Returns a string Representation of the tile.
        :return: str.  This is the string representation of the Tile.  If the tile is hidden it will return "H",
                    otherwise it should return the value of the Tile.
        """
        
        if self.hidden == True:
            return "H"
        return self.__value


    def __repr__(self):
        """ Returns the representation of the Tile """

        return self.__str__()

    def increment_mine_count(self):
        """ increments the number of mines by 1.
        :return: Nothing

         Modifications : Increments the count of the mines and __value attribute
                            to reflect the current value of the Tile.
        """
        
        self.__mine_count += 1
        self.__value = str(self.__mine_count)
    
    def set_mine_count(self, mines):
        """ Sets the number of mines around this tile.
        :param mines: The number of mines around this tile.
        :return: Nothing

            Modifications : Changes the __mine_count attribute and also updates the value to display from __str__
        """       

        self.__mine_count = mines
        self.__value = str(self.__mine_count)


    #had difficulty returning private attributes, so the next three methods were work-arounds.
    def get_mine_count(self):
        """ Returns mine_count attribute from Tile to access the private attribute from other classes
        :return: self.__mine_count, a private attribute
        """

        return self.__mine_count


    def get_value(self):
        """ Returns value attribute from Tile to access the private attribute from other classes
        :return: self.__value, a private attribute
        """
        
        return self.__value


    def set_value(self, value):
        """Sets the private attribute value of the Tile from other classes
        :return: Nothing
        """
        
        self.__value = value


    def uncover(self):
        """ Uncovers this mine.
        :return: Nothing

        Modifications : Uncovers this Tile piece.  It will uncover its neighboring pieces if necessary.
                        See rules for when and what parts to uncover.

                        Part of this method may need to wait until you've implemented the get_tile method in SuperSweeperBoardClass
        """

        # uncover tile, need to do a hidden check just so the non_mines counter doesn't double count when neighbor tiles call uncover()
        if self.hidden == True:
            self.hidden = False
            #increment the count of uncovered non-mine tiles (to know when game is won)
            self.mine_board.non_mines += 1
        #if the value of the tile is 0 (no mines), uncover each neighbor tile if they are not already uncovered
        if self.get_value() == "0":
            offset = [(1, -1), (1, 0), (1, 1), (0, -1), (0,1), (-1, -1), (-1, 0), (-1, 1)]
            for item in offset:
                    neighbor_y = self.row + item[0]
                    neighbor_x = self.col + item[1]
                    neighbor_tile = self.mine_board.get_tile(neighbor_y, neighbor_x)
                    if neighbor_tile and (neighbor_tile.hidden == True):
                        neighbor_tile.hidden = False
                        #if neighbors weren't previously uncovered, add them to the uncovered non-mines count
                        self.mine_board.non_mines += 1
                        if neighbor_tile.get_value() == "0":  #attempt to cascade
                            neighbor_tile.uncover()


class ShowMinesTile(Tile):
    """  Shows all mines around it, it is a normal tile as well

        This acts like a normal mine, but when uncovered it will show any mines around itself.
    """
    def __init__(self, mine_board, row, col):
        """ Initializes the ShowMinesTile
        :param mine_board: The board this tile is on.
        :param row: The row this tile occupies
        :param col: The col this tile occupies
        :return: Nothing
        """
        # Uses constructor from Tile
        Tile.__init__(self, mine_board, row, col)

    def uncover(self):
        """ Uncovers this tile.
        :return: Nothing

        Modifications : This will do the normal modifications of a Tile ( see above ), but will also show any mines that
                            happen to be around it.

                        Part of this method may need to wait until you've implemented the get_tile method in SuperSweeperBoardClass

        """
        # uncover tile and increment the uncovered non-mines count
        if self.hidden == True:
            self.hidden = False
            self.mine_board.non_mines += 1
        #check neighbor tiles
        offset = [(1, -1), (1, 0), (1, 1), (0, -1), (0,1), (-1, -1), (-1, 0), (-1, 1)]
        for item in offset:
                neighbor_y = self.row + item[0]
                neighbor_x = self.col + item[1]
                neighbor_tile = self.mine_board.get_tile(neighbor_y, neighbor_x)
                if neighbor_tile: #does not return None
                    #if a neighbor tile is hidden and is a mine or there are 0 mines, uncover the neighbor tile
                    if neighbor_tile.hidden == True and (isinstance(neighbor_tile, MineTile) or self.get_value() == "0"):
                        neighbor_tile.hidden = False
                        #if the uncovered tile was not a mine, increment the non_mines count
                        if not isinstance(neighbor_tile, MineTile):
                            self.mine_board.non_mines += 1
                        if neighbor_tile.get_value() == "0":
                            neighbor_tile.uncover()
       

class MineTile(Tile):
    """ This is a Mine Tile.  It has all the same functionality of a tile, but the value to display is an asterisk * """
    def __init__(self, mine_board, row, col):
        """ Initializes the MineTile
        :param mine_board: The board this tile is on.
        :param row: The row this tile occupies
        :param col: The col this tile occupies
        :return: Nothing

        Modifications : Initializes the Tile the same as the superclass Tile, but sets the __value to be an *
        """
        # uses the constructor from Tile, but call the set value method to update the private value from this class.
        Tile.__init__(self, mine_board, row, col)
        self.set_value("*")


class SuperSweeperBoard(object):
    """ Classic game of minesweeper with some new elements.
            This class contains the following information
                rows : int.  The number of rows on the board
                cols : int.  The number of columns on the board.
                mines : int.  The number of mines on the board.
                __board : list.  Contains a list of rows on the board.  Each row is a list of the tiles.
                game_state : int.  This is the state of the game.   0 : playing
                                                                    1 : Game Over Playing Won
                                                                    2 : Game Over Player Lost
                """
    

    def __init__(self, rows=5, columns=5, mine_count=10, show_mine_tiles=0):
        """  Initializes the Minesweeper Board
        :param rows: int  The Number of rows in our board
        :param columns: int.  The number of columns on our board.
        :param mine_count: int.  The number of mines on our board.
        :param show_mine_tiles:  The number of show mine tiles to place on our board.

        Modifications : __init__ will setup the attributes to the beginning state of the game.
                        The init will setup the __board for the number of rows and columns.
                        Each element in __board is a Tile.
                        There will be mines placed randomly on the board corresponding to the mine_count number.
                        There will be as many ShowMineTiles randomly placed on the board as parameter show_mine_tiles
                        It should also set the # of mines for each tile based on how many mines are around it.
                        The Gamestate should be set to the initial game state.

        Error Handled : if rows is less than 3 then it will be set to 3.
                        if rows is greater than 12 then it will be set to 12
                        if columns is less than 3 then it will be set to 3
                        if columns is greater than 12 then it will be set to 12
                        mine_count must be greater than 0, and less or equal to the number of positions available.
                            If the board is 10x10, then there are 100 places to put a mine.
                            ( although it'll be a short game ).
                            If mine_count violates either then it is set to the appropriate value.
                            if it's less than 1, then it is assigned 1.
                            If it is greater than the positions available then it is set to the # of cells
                        show_mine_tiles not be less than 0, but it can be zero.
                            If it is greater than the # of positions left after the mines have been placed, then it is
                            set to the number of positions left.

        :return: Nothing
        """

        self.non_mines = 0
        self.game_state = 0
        
        # check for valid entry or set other values for rows, columns, mine_count, and show_mine_tiles
        if rows < 3:
            self.rows = 3
        elif rows > 12:
            self.rows = 12
        else:
            self.rows = rows
        
        if columns < 3:
            self.columns = 3
        elif columns > 12:
            self.columns = 12
        else:
            self.columns = columns

        self.total_size = self.rows * self.columns

        if mine_count < 1:
            self.mine_count = 1
        elif mine_count > self.total_size:
            self.mine_count = self.total_size
        else:
            self.mine_count = mine_count
        
        if show_mine_tiles < 0:
            self.show_mine_tiles = 0
        elif show_mine_tiles > (self.total_size - self.mine_count):
            self.show_mine_tiles = (self.total_size - self.mine_count)
        else:
            self.show_mine_tiles = show_mine_tiles
                

        #build the board as a 2D list, where each inner list is a list of tiles for a row
        self.__board = []
        for i in range(self.rows):
            self.y = []
            for j in range(self.columns):
                self.y.append(Tile(self,i,j))
            self.__board.append(self.y)
        self.__place_mines(self.mine_count)
        self.__set_mine_counts()
        self.__place_specials(self.show_mine_tiles)
        
        

    def __str__(self):
        """  Returns a String representation of the object
        :return: str.  String representation of the board.

        This will have the following example format for 5 columns and 4 rows

                  Minesweeper Board

       R\C   0  1  2  3  4
           _______________
         0|  H  H  H  H  H
         1|  H  H  H  H  H
         2|  H  H  H  H  H
         3|  H  H  H  H  H
        """

        #only displays lists of rows, needs updating
        column_list = [str(i) for i in range(self.columns)]
        board_str = "{}\nR\C  {}\n    {}".format("Minesweeper Board\n", "  ".join(column_list), "-" * self.columns * 3)
        row_list = []
        for row in self.__board:
            tile_list = []
            for tile in row:
                tile_list.append(str(tile))
            row_list.append(tile_list)
        for index, row in enumerate(row_list):
            board_str = board_str + "\n" + "  " + str(index) + "| " + "  ".join(row)
        
        return board_str


    def __repr__(self):
        """ Returns the representation of the Board """
        return self.__str__()

    def get_tile(self, y, x):
        """  Returns what is at the Position X and Y, if it is outside the board, then None is returned
        :param y: int.  Row of the tile begin requested
        :param x: int.  Column of the tile being requested.
        :return: A from that position or None if values are outside of the board.

        Errors Handled : if the position being requested is outside of the board, then None is returned.
        """
        
        if y in range(self.rows) and x in range(self.columns):
            return self.__board[y][x]
        return None        

    def __place_mines(self, mines):
        """  Places the required number of mines for the user
        :param mines: int.  The number of mines to place
        :return: Nothing

            Modifications : This will change the __board and place the proper number of mines on the board.
                            Do not place a mine where one has already been placed.
        """

        #randomly place mines; if a mine already exists in randomly chosen location, choose again until all
        #mines are placed.
        y, x, rand_count = 0, 0, 0
        while rand_count < mines:
            y = random.randint(0, self.rows - 1)
            x = random.randint(0, self.columns - 1)
            tile = self.get_tile(y, x)
            if not isinstance(tile, MineTile):  #if the tile has not already been set as a mine tile
                self.__board[y][x] = MineTile(self, y, x)
                rand_count += 1
        
    def __place_specials(self, show_mines):
        """ Places all special show mine tiles
        :param show_mines: int.  The number of show_mines to place on the board.
        :return: Nothing

            Modifications : changes the __board and places random ShowMineTiles on the board.
                            There should be as many ShowMineTiles as the variable show_mines passed.
                            If there is a mine in a spot, then the ShowMineTile will not replace a Mine
        """

        # randomly place show_mine tiles similar to above
        y, x, rand_count = 0, 0, 0
        while rand_count < show_mines:
            y = random.randint(0, self.rows - 1)
            x = random.randint(0, self.columns - 1)
            tile = self.get_tile(y, x)
            if not isinstance(tile, MineTile):
                show_mines_tile = ShowMinesTile(self, y, x)
                #must update the mine count for the show mines tile with the tile count,
                #otherwise it would be set to initialization of 0.
                show_mines_tile.set_mine_count(tile.get_mine_count())
                self.__board[y][x] = show_mines_tile
                rand_count += 1

    def __set_mine_counts(self):
        """  Sets the counts of the mines around the cell
        :return: Nothing

            Modifications : For each tile that is not a mine it sets the number of mines around it.
        """
        # Go through each tile in the board
        offset = [(1, -1), (1, 0), (1, 1), (0, -1), (0,1), (-1, -1), (-1, 0), (-1, 1)]
        for row in self.__board:
            for tile in row:
                #if the tile is not a mine, go through the offset list to find neighbor tiles
                if not isinstance(tile, MineTile):
                    for item in offset:
                        neighbor_y = tile.row + item[0]
                        neighbor_x = tile.col + item[1]
                        neighbor_tile = self.get_tile(neighbor_y, neighbor_x)
                        if neighbor_tile: #if not off the board                                  
                            #and if the tile is a mine, increment the mine count for the tile
                            if isinstance(neighbor_tile, MineTile):
                                tile.increment_mine_count()
                    
        

    def __show_all(self):
        """ Shows all the cells.  Sets the hidden attribute to True for each cell.
            This occurs when the game is over """
        
        for row in self.__board:
            for tile in row:
                tile.hidden = False

    #unused
    def __check_win(self):
        """ Returns True if the game has been won ( all non mines are uncovered ) False otherwise """
        if self.game_state == 1:
            return True
        return False

    def uncover_cell(self, y, x):
        """ Uncovers the cell as position given.
        :param y: int.  The row being uncovered.
        :param x: int.  The column being uncovered.
        :return: None

            Modifications : Uncovers the cell.  If the cell is a mine, then the game is over and all the cells are
                                    uncovered.  Set the gamestate to the proper value.
                            If the cell is a tile and has zero mines around it, then the tiles around it are uncovered
                            Once the cell is uncovered, if there are no more non mine tiles uncovered
                                    then the game must be over.  Show the entire board and set the gamestate to the
                                    proper value.
        """

        tile = self.get_tile(y, x)
        if isinstance(tile, MineTile):
            tile.hidden = False
            self.game_state = 2  #lose the game immediately when mine is uncovered
            self.__show_all()
            print(self)
        else:
            tile.uncover()
        
        if self.non_mines >= (self.total_size - self.mine_count):
            self.game_state = 1 #win the game when all non-mines are revealed
            self.__show_all()
            print(self)



def get_user_choice(prompt, valid_choices, error_text):
    """Displays a prompt asking the user for input, displays an error and loops back if input is
       invalid.
       :param prompt: string displayed to user asking for input
       :param valid_choices: a collection of valid inputs
       :param error_text: string displayed to the user if the input is invalid.
       :return user_choice: the valid input given by the user.
       """    

    user_choice = input(prompt).upper()
    while user_choice not in valid_choices:
        print(error_text)
        user_choice = input(prompt).upper()
    return user_choice


def get_user_tiles(prompt):
    """Gets the board rows and columns or tile row, column to uncover from the user
    :param prompt: string displayed to the user
    :return: rows, columns - a tuple containing a valid row, column pair

    Error-checking: makes sure entries are integers separated by a comma.
    """
    
    while True:
        try:
            size = input(prompt)
            size_list = size.split(",")
            rows = int(size_list[0])
            columns = int(size_list[1])
            return rows, columns
        except IndexError:
            print("You must enter two values for rows, columns")
        except ValueError:
            print("You must enter two integers, separated by a comma(,).")


def get_user_specials(prompt):
    """Prompts the user for number of mines or show_mine tiles to place.
    :param prompt: string displayed to user
    :return: tiles, the number of mines or show_mines to place

    Error checking: makes sure the entry is an integer. Values out of range are corrected by
    the appropriate classes, not here.
    """
    
    while True:
        try:
            tiles = int(input(prompt))
            return tiles
        except ValueError:
            print("You must enter an integer value.")


def user_selection(board):
    """Gets the tile the user would like to uncover.
    :param board: the board containing the tiles to uncover.
    :return: y, x - a tuple of row, column for the tile to uncover

    Error checking: makes sure the rows and columns entered are greater than 0 and less or equal to rows,
    columns on the board.
    """
    
    while True:
        tile = get_user_tiles("Enter a row and column to uncover, separated by a comma(,): ")
        y = tile[0]
        x = tile[1]
        if y > board.rows or y < 0 or x > board.columns or x < 0:
            print("Value entered is out of range. Try again.")
        else:
            return y, x
        
        

if __name__ == "__main__":

    play_again = True
    valid_choices = ["Y", "YES", "N", "NO"]

    while play_again:
        print("Super Sweeper\n")
        board_size = get_user_tiles("Enter the number of rows and columns for the board, separated by a comma: ")
        #split up the returned tuple to get the rows and columns for the board
        board_rows = board_size[0]
        board_columns = board_size[1]
        mines = get_user_specials("Enter the number of mines: ")
        show_mines = get_user_specials("Enter the number of show mines: ")
        
        board = SuperSweeperBoard(board_rows, board_columns, mines, show_mines)

        while board.game_state == 0:
            print(board)
            print()
            user_cell = user_selection(board) #get the tile to uncover
            user_y = user_cell[0]
            user_x = user_cell[1]
            board.uncover_cell(user_y, user_x) #and uncover it
            
            #uncover_cell() also updates the game state
            if board.game_state == 1:  
                print("You win!")
            elif board.game_state == 2:
                print("You hit a mine. GAME OVER.")
                
        playing = get_user_choice("Would you like to play again? ", valid_choices, "You must enter Y or N")
        if playing in ["N", "NO"]:
            play_again = False
            print("Thanks for playing.")
            
    

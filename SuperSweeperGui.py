from graphics import *
from SuperSweeper import *
import time
import os

WIDTH = 400
HEIGHT = 500
ROWS = 10
COLS = 10
MINES = 20
SHOW_MINES = 3
TILE_WIDTH = 40
TILE_HEIGHT = 40

BLANK_SQUARE_FILE = "images" + os.sep + "BlankSquare.gif"

SQUARE = Image(Point(0,0), "images" + os.sep + "BlankSquare.gif")
EXPLOSION = Image(Point(0,0), "images" + os.sep + "explosion.gif")
BLANK = Image(Point(0,0), "images" + os.sep + "blank.gif")
MINE = Image(Point(0,0), "images" + os.sep + "mine.gif")
WIN = Image(Point(0,0), "images" + os.sep + "win.gif")

def return_point(row, col):
    """ Given a row and a column it calculates the point for the graphic on the screen.
    :param row:
    :param col:
    :return:
    """
    return Point(col * TILE_WIDTH + TILE_WIDTH / 2, row * TILE_HEIGHT + TILE_HEIGHT / 2)


def return_new_grid():
    """  returns a list of rows and columns of the images to display.
    :return:
    """
    grid = []
    for row in range(ROWS):
        row_list = []
        for col in range(COLS):
            img = SQUARE.clone()
            img.anchor = return_point(row, col)
            row_list.append(img)
        grid.append(row_list)
    return grid

def draw_board(board, gr, text_list):
    for row in board:
        for col in row:
            col.undraw()
            col.draw(gr)
    for item in text_list:
        item.undraw()
        item.draw(gr)

def row_col_at_point(point):
    """  Given a point on the screen it determines the row and column for that point.
    :param point:
    :return:
    """
    row = int(point.y // TILE_HEIGHT)
    col = int(point.x // TILE_WIDTH)
    return row, col

def update_board(grid, game, textlist):
    for item in textlist:
        item.undraw()
    textlist.clear()

    for row in range(ROWS):
        for col in range(COLS):
            cell = game.get_tile(row, col)
            if cell.hidden:
                img = SQUARE.clone()
                img.anchor = grid[row][col].anchor
                grid[row][col].undraw()
                grid[row][col] = img
            elif type(cell) == MineTile:
                img = MINE.clone()
                img.anchor = grid[row][col].anchor
                grid[row][col].undraw()
                grid[row][col] = img
            else:
                img = BLANK.clone()
                img.anchor = grid[row][col].anchor
                grid[row][col].undraw()
                grid[row][col] = img
                txt = Text(img.anchor, str(cell))
                textlist.append(txt)

def main():
    win = GraphWin("Mine Sweeper", WIDTH, HEIGHT, autoflush=False)
    win.setBackground("white")

    playing = True

    while playing:

        # Start Playing Minesweeper
        game_board = SuperSweeperBoard(ROWS, COLS, MINES, SHOW_MINES)
        board_grid = return_new_grid()
        text_strings = []
        draw_board(board_grid, win, text_strings)
        while game_board.game_state == 0:

            mouse = win.checkMouse()
            if mouse is not None:
                row, col = row_col_at_point(mouse)
                game_board.uncover_cell(row, col)
                update_board(board_grid, game_board, text_strings)
                draw_board(board_grid, win, text_strings)

            win.update()
            time.sleep(0.02)

        # They've either won or lost.
        if game_board.game_state == 2:  # They lost
            end_image = EXPLOSION.clone()
            msg = "You LOST"
        else:
            end_image = WIN.clone()
            msg = "You WON"
        end_image.anchor.y = 450
        end_image.anchor.x = 60
        end_image.draw(win)

        text = Text(Point(200, 450), msg + "  Play Again (Y/N)")
        text.draw(win)
        key = win.getKey().upper()
        while key != "Y" and key != "N":
            key = win.getKey().upper()

        end_image.undraw()
        text.undraw()

        if key == "N":
            playing = False


try:
    main()
except GraphicsError as g:
    print("A Graphics Error occurred ", g)
